




export function formatyear(date) {

    const dateTime = new Date(date);
    const YY = dateTime.getFullYear();
    return YY
}

export function formatDate(date) {

    const dateTime = new Date(date);
    const YY = dateTime.getFullYear();
    const MM = dateTime.getMonth()+1;
    const DD = dateTime.getDate();

    let HH = dateTime.getHours();
    let mm = dateTime.getMinutes();
    let ss = dateTime.getSeconds();
    if (HH <= 9) {
        HH = '0' + HH
    }
    if (mm <= 9) {
        mm = '0' + mm
    }
    if (ss <= 9) {
        ss = '0' + ss
    }
    return YY + '-' + MM + '-' + DD + ' ' + HH + ':' + mm + ':' + ss
}

export default {
    formatyear: formatyear,
    formatDate
}
