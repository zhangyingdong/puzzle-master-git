import Vue from "vue";
import router from "./router";
import store from "./store/";
import dateManager from "./dateManager";
import not from "./not";
import App from "./App.vue";

// 接口
import axios from "./api/axios";
window.axios = axios;

Vue.prototype.dateManager = dateManager
Vue.prototype.notServe = not
Vue.prototype.tVue = Vue
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

if (process.env.NODE_ENV == "development") {
  console.log("development mode");
  //开启debug模式
  Vue.config.debug = true;
}

new Vue({
  router,
  store,
  dateManager,
  render: h => h(App),
  data:{
    eventHub: new Vue()
  }
}).$mount("#app");
