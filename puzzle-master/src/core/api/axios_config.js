export default {
  // 默认请求的接口
  url: "/",
  // 服务器地址
  baseURL: "",
  // 设置超时时间 ms
  timeout: 60 * 1000,
  // 是否跨站点访问控制请求
  withCredentials: false // default
};

// export default {
//   // 默认请求的接口
//   url: "/",
//   // 测试服务器地址
//   baseURL: "https://webapi2.test.goworkla.cn",
//   agentURL: "https://agent.test.goworkla.cn",
//
//   clientId : '59c0e143555818d2af8e4a81',
//
//   // 设置超时时间 ms
//   timeout: 60 * 1000,
//   // 是否跨站点访问控制请求
//   withCredentials: false // default
// };
