export default [
    {
        name: "AA",
        path: "/A/AA",
        meta: {
            name: "AAAAAAAAAAAA"
        },
        component: () =>
            import(/* webpackChunkName: "views/testHome" */ `../views/AA`)
    },
    {
        name: "order",
        path: "/order/orderPage",
        meta: {
            name: "工单"
        },
        component: () =>
            import(/* webpackChunkName: "views/testHome" */ `../views/order`)
    },
    {
        name: "orderDetail",
        path: "/order/orderDetail",
        meta: {
            name: "工单详情"
        },
        component: () =>
            import(/* webpackChunkName: "views/testHome" */ `../views/orderDetail`)
    }
];
